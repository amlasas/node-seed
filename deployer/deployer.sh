#!/bin/bash

sed -i "s/node-seed:.*/node-seed:$1/g" docker-compose.override.yml
cp example.env prod.env
sed -i "s/MYSQL_ROOT_PASSWORD=.*/MYSQL_ROOT_PASSWORD=$2/g" prod.env
sed -i "s/MYSQL_DATABASE=.*/MYSQL_DATABASE=$3/g" prod.env
sed -i "s/MYSQL_USER=.*/MYSQL_USER=$4/g" prod.env
sed -i "s/MYSQL_PASSWORD=.*/MYSQL_PASSWORD=$5/g" prod.env

sed -i "s/DB_NAME=.*/DB_NAME=$3/g" backend.env
sed -i "s/DB_USERNAME=.*/DB_USERNAME=$4/g" backend.env
sed -i "s/DB_PASS=.*/DB_PASS=$5/g" backend.env




docker-compose up -d