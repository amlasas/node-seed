import { CommonUtil } from './utils/common.util';
import { Sequelize, Op } from 'sequelize';
import { logger } from './logger';
import * as sqlFormatter from 'sql-formatter';
import { config } from './config';

const bd = config.getBd();
console.log('este es el host',bd.host)
export const sequelize = new Sequelize({
  host: bd.host,
  dialect: bd.dialect,
  database: bd.database,
  username: bd.username,
  password: bd.password,
  logging: (query: string) => {
    if (CommonUtil.isDev) {
      logger.trace(sqlFormatter.format(query));
    }
  },
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});